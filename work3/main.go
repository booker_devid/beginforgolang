/*
Программа является примером не доконца декомпозированного кода.

То есть данный код становится сложнее чистать из-за его размера
Правильно написанный код, чаще всего разделен на функции/процедуры,
которые помогают разделить код на смысловые части и следовать DRY (Don't repeat your)
Практика DRY говорит о том, что код, который используется больше одного раза
должен быть вынесен в функцию.

Программа должна выполнять: поиск наименьшего числа, поиск наибольшего числа, поиск суммы все четных чисел и поиск сумм всех не четных чисел
*/
package main

import (
	"errors"
	"log"
)

func FindMin(nums []int) (int, error) {
	if len(nums) == 0 {
		return 0, errors.New(`list numbers is empty`)
	}

	minNumber := nums[0]
	for _, num := range nums {
		if num < minNumber {
			minNumber = num
		}
	}

	return minNumber, nil
}

func main() {
	listNumbers1 := []int{1, 2, 3, 4, 5, 12, -30, 41, -56, 65, -72, 87, -98, 102, -99}
	listNumbers2 := []int{11, 22, 33, 44, 55, 66, 77, 88, 99, 111, 222, 333, 444, 555, 666, 777, 888, 999}
	listNumbers3 := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17}

	// (пример с применением практики DRY, но можно лучше)
	// Поиск наименьшего числа
	minNumList1, err := FindMin(listNumbers1)
	if err != nil {
		log.Fatal(err)
	}
	minNumList2, err := FindMin(listNumbers2)
	if err != nil {
		log.Fatal(err)
	}
	minNumList3, err := FindMin(listNumbers3)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf(
		"Минимальные числа: \n\tlist1: %d\n\tlist2: %d\n\tlist3: %d",
		minNumList1,
		minNumList2,
		minNumList3,
	)

	// (пример без применением практики DRY)
	// Поиск наибольшего числа
	if len(listNumbers1) == 0 {
		log.Fatal(`list numbers is empty`)
	}

	maxNumList1 := listNumbers1[0]
	for _, num := range listNumbers1 {
		if num > maxNumList1 {
			maxNumList1 = num
		}
	}

	if len(listNumbers2) == 0 {
		log.Fatal(`list numbers is empty`)
	}

	maxNumList2 := listNumbers2[0]
	for _, num := range listNumbers2 {
		if num > maxNumList2 {
			maxNumList2 = num
		}
	}

	if len(listNumbers3) == 0 {
		log.Fatal(`list numbers is empty`)
	}

	maxNumList3 := listNumbers3[0]
	for _, num := range listNumbers3 {
		if num > maxNumList3 {
			maxNumList3 = num
		}
	}

	log.Printf(
		"Максимальные числа: \n\tlist1: %d\n\tlist2: %d\n\tlist3: %d",
		maxNumList1,
		maxNumList2,
		maxNumList3,
	)

	// Требуется дописать
	// Поиск суммы четных чисел
	// Поиск суммы не четных чисел
}
