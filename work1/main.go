/*
Программа должна выполнять поиск минимального числа.
*/
package main

import "log"

func main() {
	numbers := []string{"1", "-1", "1000", "-798", "-0.9"}

	minNumber := numbers[0]
	for _, num := range numbers {
		if num < minNumber {
			minNumber = num
		}
	}

	log.Printf(`Min number - %s`, minNumber)
}
